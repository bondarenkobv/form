import FormHandler from '../FormHandler.js'
// import Loader from '../../loader/Loader'

FormHandler.create('contacts')
	.success(event => {
		console.log('success', event)
	})
	.error(event => {
		console.log('error', event)
		// Notification.init(event.detail.message, 'alert--danger')
	})
	.loading(event => {
		console.log('loading', event)
		// event.currentTarget.insertAdjacentHTML('beforeend', Loader.default)
	})
	.finally(event => {
		console.log('finally', event)
		// event.currentTarget.querySelector('.guru-loader').remove();
	})
