import FORM from './FormSubmit'
import './handlers/contacts'

document.addEventListener("DOMContentLoaded", function(){
  FORM.methods.init(document.querySelectorAll('.form--js'))
})