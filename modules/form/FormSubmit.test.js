import Form from './FormSubmit.js'
import jsdom from 'jsdom'
const { JSDOM } = jsdom,
	dom = new JSDOM(
		`<form id="contacts-form" method="POST" action="/success" class="form form--js"><div class="form__items"> <div class="form__item"> <div class="form__group"> <label for="form_6175761db944c" class="form__label">LBL_Search_By_Keyword</label> <input type="text" id="form_6175761db944c" class="form__field" name="email" value="2"> <span class="form__error"></span></div></div><div class="form__item"> <div class="form__group"> <label for="form_6175761db9836" class="form__label">LBL_Search_By_Keyword</label> <input type="text" id="form_6175761db9836" class="form__field" name="phone" value="3"> <span class="form__error"></span></div></div><div class="form__item"> <div class="form__group"> <label for="form_6175761db985c" class="form__label">LBL_Search_By_Keyword</label> <input type="text" id="form_6175761db985c" class="form__field" name="test" value="3"> <span class="form__error"></span></div></div></div><div class="form__items"> <div class="form__item"> <button type="submit" class="form__btn">Отправить</button>
		<button type="reset" class="form__btn btn--reset refresh--request">Сброс</button> 
		</div></div></form>`
	),
	document = dom.window.document,
	form = document.getElementById('contacts-form')

global.CustomEvent = dom.window.CustomEvent

jest.mock('./services/request.services.js')

describe('Testing Form Submission', () => {
	test('Checks and removes classes is--submit, form--init', () => {
		Form.methods.setDefaultStateForm(form)
		expect(form.classList.contains('form--init')).toEqual(false)
		expect(form.classList.contains('is--submit')).toEqual(false)
	})

	test('form contains class form--init', () => {
		Form.methods.init(document.querySelectorAll('.form--js'))
		expect(form.classList.contains('form--init')).toEqual(true)
	})

	test('Check button lock on form submit', () => {
		Form.methods.blockSubmitChange(form, true)
		expect(form.classList.contains('is--submit')).toEqual(true)
	})

	test('Check button unlock when submitting form', () => {
		Form.methods.blockSubmitChange(form, false)
		expect(form.classList.contains('is--submit')).toEqual(false)
	})

	test('Check automatic form submission', () => {
		expect(Form.methods.formSubmitting(form)).not.toBe(undefined)
	})
})

describe('Submit request success', () => {
	const responseSuccess = {
		email: 'test@mail.ru',
		phone: '33333313213',
	}

	it('Response success handler', async () => {
		form.action = '/success'
		expect.assertions(1)
		const successCallback = jest.fn()
		form.addEventListener(`form-${form.id}-success`, successCallback)
		await Form.submit.request.send(form, responseSuccess)
		expect(successCallback).toHaveBeenCalledTimes(1)
	})

	it('Response success handler details', done => {
		form.action = '/success'
		expect.assertions(1)
		form.addEventListener(`form-${form.id}-success`, response => {
			try {
				expect(response.detail).toEqual(responseSuccess)
				done()
			} catch (error) {
				done(error)
			}
		})
		Form.submit.request.send(form, responseSuccess)
	})
})

describe('Submit request error', () => {
	const responseError = {
		response: {
			data: {
				data: {
					email: '123',
					phone: '123',
				},
			},
		},
	}

	it('Response error handler', async () => {
		form.action = '/error'
		expect.assertions(1)
		const successCallback = jest.fn()
		form.addEventListener(`form-${form.id}-error`, successCallback)
		await Form.submit.request.send(form, responseError)
		expect(successCallback).toHaveBeenCalledTimes(1)
	})

	it('Response error handler details', done => {
		form.action = '/error'
		expect.assertions(1)
		form.addEventListener(`form-${form.id}-error`, response => {
			try {
				expect(response.detail).toEqual(responseError.response.data)
				done()
			} catch (error) {
				done(error)
			}
		})
		Form.submit.request.send(form, responseError)
	})
})