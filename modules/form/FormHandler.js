const FormHandler = {
	form: null,

	create(formId) {
		this.form = document.getElementById(formId)
		return {
			success(callback) {
				if (this.form) return this.form.addEventListener(`form-${formId}-success`, callback)
				return this
			},

			error(callback) {
				if (this.form) return this.form.addEventListener(`form-${formId}-error`, callback)
				return this
			},

			loading(callback) {
				if (this.form) return this.form.addEventListener(`form-${formId}-loading`, callback)
				return this
			},

			finally(callback) {
				if (this.form) return this.form.addEventListener(`form-${formId}-finally`, callback)
				return this
			},
		}
	},
}

export default FormHandler
