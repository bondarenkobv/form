import { sendRequest } from './services/request.services'
const Form = {
	methods: {
		init(arrayForms) {
			if (!arrayForms) return false

			arrayForms.forEach(form => {
				if (!Form.methods.checkBeforeInitForm(form)) return false
				
				Form.methods.setDefaultStateForm(form)
				form.addEventListener('submit', Form.methods.submitForm)
				Form.methods.formInitialized(form)
				Form.methods.formSubmitting(form)
				Form.handlerResetBtn(form)
			})
		},

		checkBeforeInitForm(form) {
			if (form.hasAttribute('action') && form.getAttribute('action') === '') return false

			if (form.hasAttribute('method') && form.getAttribute('method') === '') return false

			return true
		},

		setDefaultStateForm(form) {
			if (form.classList.contains('is--submit')) {
				form.classList.remove('is--submit')
			}

			if (form.classList.contains('form--init')) {
				form.classList.remove('form--init')
			}
		},

		formInitialized(form) {
			form.classList.add('form--init')
		},

		formSubmitting(form) {
			if (form.classList.contains('form--submitting')) return Form.submit.request.send(form)
		},

		submitForm(event) {
			try {
				event.preventDefault()
				const form = event.currentTarget
				Form.methods.validate.reset(form)
				Form.submit.request.send(form, Form.methods.getFormData(form))
			} catch (Error) {
				// console.log(Error)
			}
		},

		getFormData(form) {
			return new FormData(form)
		},

		blockSubmitChange(form, blockSubmit) {
			if (blockSubmit) return form.classList.add('is--submit')
			return form.classList.remove('is--submit')
		},

		validate: {
			reset(form) {
				form.querySelectorAll(`.input--error`).forEach(inputError => {
					inputError.classList.remove('input--error')
					inputError.nextElementSibling.innerHTML = ''
				})
			},

			valid(errors, form) {
				const inputsErrors = Object.keys(errors)
				if (inputsErrors) {
					inputsErrors.forEach(inputError => {
						const selector = form.querySelector(`[name='${inputError}']`)
						selector.classList.add('input--error')
						selector.nextElementSibling.innerHTML = errors[inputError]
					})
				}
			},
		},

		loader(form, data) {
			form.dispatchEvent(
				new CustomEvent(`form-${form.id}-loading`, {
					detail: { form: form, data: data },
					bubbles: true,
				})
			)
		},
	},

	submit: {
		request: {
			async send(form, data) {
				Form.methods.loader(form, data)
				Form.methods.blockSubmitChange(form, true)
				return await sendRequest(form.method, form.action, data)
					.then(response => {
						Form.submit.response.success(form, response)
					})
					.catch(error => {
						if (error.response.data !== undefined) {
							Form.submit.response.error(form, error.response.data)
						}
					})
					.finally(() => {
						Form.submit.response.finally(form)
					})
			},
		},
		response: {
			success(form, response) {
				form.dispatchEvent(
					new CustomEvent(`form-${form.id}-success`, {
						detail: response,
						bubbles: true,
					})
				)
			},
			error(form, error) {
				Form.methods.validate.valid(error.data, form)
				form.dispatchEvent(
					new CustomEvent(`form-${form.id}-error`, {
						detail: error,
						bubbles: true,
					})
				)

				// console.log('ERROR!', error)
			},
			finally(form) {
				Form.methods.blockSubmitChange(form, false)
				form.dispatchEvent(
					new CustomEvent(`form-${form.id}-finally`, {
						detail: { form: form },
						bubbles: true,
					})
				)
			},
		},
	},

	handlerResetBtn(form) {
		const resetBtn = form.querySelector('[type="reset"]')
		if (resetBtn && resetBtn.classList.contains('refresh--request')) {
			resetBtn.addEventListener('click', function() {
				Form.submit.request.send(form)
			})
		}
	},
}
export default Form
