export const sendRequest = (method, action, data) => {
	return new Promise((resolve, reject) => {
		switch (action) {
			case '/success':
				resolve(data)
				break
			case '/error':
				reject(data)
				break
			default:
				reject(data)
				break
		}
	})
}
