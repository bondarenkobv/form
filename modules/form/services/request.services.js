import axios from '../../../utils/axios'

export const sendRequest = (formMethod, formAction, formData) => {
	return axios({
		method: formMethod,
		url: formAction,
		data: formData,
	})
}
